variable "name" {
	description = "name of vpc"
}

variable "cidr" {
	description = "CIDR of vpc"
}

variable "public_subnet" {
}

variable "enable_dns_hostnames" {
	default = true
}

variable "enable_dns_support" {
	default = true
}

output "public_subnet_id" {
	value ="${aws_subnet.public.id}"
}

output "vpc_id" {
	value = "${aws_vpc.app_vpc.id}"
}

#you can add sensitive = true
output "cidr" {
	value = "${aws_vpc.app_vpc.cidr_block}""
}