#to load module, use terraform get to store it in .terraform/modules 
#to update, terraform get -update

#VPC with Internet access internet routing and public subnet
resource "aws_vpc" "app_vpc" {
  cidr_block           = "${var.cidr}"
  enable_dns_hostnames = "${var.enable_dns_hostnames}"
  enable_dns_support   = "${var.enable_dns_support}"

  tags {
    Name = "${var.name}"
  }
}

resource "aws_internet_gateway" "app_vpc" {
  vpc_id = "${aws_vpc.app_vpc.id}"

  tags {
    Name = "${var.name}-ig"
  }
}

resource "aws_route" "internet" {
  route_table_id         = "${aws_vpc.app_vpc.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.app_vpc.id}"
}

resource "aws_subnet" "public" {
  vpc_id                  = "${aws_vpc.app_vpc.id}"
  cidr_block              = "${var.private_subnet}"

  tags {
    Name = "${var.name}-public"
  }
}

