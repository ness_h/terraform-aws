variable "access_key" {
	description = "AWS Access key"
}

variable "secret_key" {
	description = "AWS Secret key"
}

variable "region" {
	description = "AWS region"
	default = "us-east-1"
}

#should be created
variable "key_name" {
	description = "key pair name"
}

variable "Availibility_zones" {
	description = "AWS AZs"
	default = ["us-east-1a","us-east-1b"]
}

variable "ami" {
	type = "map"
	default = {
	us-east-1 = "ami-xxx"
	us-west-1 = "ami-yyy"
	}
	description = "AMIs"
}

variable "security_group" {
	type = "list"
	description = "List of SG"
	default = ["sg-xx","sg-yy","sg-zz"]
}

variable "instance_type" {
	description = "instance type"
	default = "t2.micro"
}

variable "instance_ips" {
	default = ["10.0.1.xx", "10.0.1.xx"]
}

variable "env" {
	default = "development"
}