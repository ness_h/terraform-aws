#commands to use:
#terraform validate (checks syntax and validate your configuration files)
#terraform fmt (formats your configuration files)
#terraform init (get provider)
#terraform plan (shows us what's going to happen when applying this configuration)
#terraform apply (build our stack) 
#terrafrom output (show stack output)
#terraform show (show list of ressources and attributes)
#terraform output -json (JSON formatting)
#terraform graph > web.dot (graphing the stack)
#terraform destroy (destroying the stack)

provider "aws" {
	access_key = "${var.access_key}"
	secret_key = "${var.secret_key}"
	region = "${var.region}"
	alias = "aws-1"
}

#Reusable bundles of ressources
#source can be path in ur local machine or git url 
module "vpc" {
    #source = "git::https://github.com/xx"
    #source = namespace/name/provider if referencing a registery module
	source = "./vpc"
	name = "app"
	cidr = "10.0.0.0/16"
	public_subnet = "10.0.1.0/24"
	enable_dns_hostnames = false
	providers = {
		"aws" = "aws.aws-1"
	}
}

#to access first instance : aws_instance.server.0
#lookup(map,key)
ressource "aws_instance" "server" {
	ami = "${lookup(var.ami,var.region)}"
	instance_type = "${var.instance_type}"
	private_ip = "${var.instance_ips[count.index]}"
	key_name = "${var.key_name}"
	subnet_id= "${module.vpc.public_subnet_id}"
    associate_public_ip_adress = true
    user_data = "${file("files/bootstrap.sh")}"
    vpc_security_group_ids = [ "${aws_security_group.app_sg.id}" ]
    count = "${var.env == "production" ? 4 : 2}""
}

resource "aws_elb" "app" {
  name = "app-elb"

  subnets         = ["${module.vpc.public_subnet_id}"]
  security_groups = ["${aws_security_group.app_inbound_sg.id}"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

#to get all IDs of EC2s instances
  instances = ["${aws_instance.app.*.id}"]
}

resource "aws_security_group" "app_inbound_sg" {
  name        = "app-inbound"
  description = "Allow HTTP from Anywhere"
  vpc_id      = "${module.vpc.vpc_id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "app_sg" {
  name        = "app_host"
  description = "Allow SSH & HTTP to app hosts"
  vpc_id      = "${module.vpc.vpc_id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP access from the VPC
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${module.vpc.cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}